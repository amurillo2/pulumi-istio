import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";

export namespace commons {
    export interface WorkloadSelector {
        matchLabels: {
            [labels: string]: pulumi.Input<any>;
        };
    }
}

export namespace AuthorizationPolicy {
    export interface AuthorizationPolicyInput {
        metadata?: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
        spec: {
            selector?: pulumi.Input<commons.WorkloadSelector>;
            rules?: pulumi.Input<Rule>[];
            action?: pulumi.Input<Action>;
        };
    }

    interface Rule {
        from?: pulumi.Input<From>[];
        to?: pulumi.Input<To>[];
        when?: pulumi.Input<Condition>[];
    }

    interface Condition {
        key: pulumi.Input<string>;
        values?: pulumi.Input<string>[];
        notValues?: pulumi.Input<string>[];
    }

    interface From {
        source?: pulumi.Input<Source>;
    }

    interface Source {
        principals?: pulumi.Input<string>[];
        notPrincipals?: pulumi.Input<string>[];
        requestPrincipals?: pulumi.Input<string>[];
        notRequestPrincipals?: pulumi.Input<string>[];
        namespaces?: pulumi.Input<string>[];
        notNamespaces?: pulumi.Input<string>[];
        ipBlocks?: pulumi.Input<string>[];
        notIpBlocks?: pulumi.Input<string>[];
    }

    interface To {
        operation?: pulumi.Input<Operation>;
    }

    interface Operation {
        hosts?: pulumi.Input<string>[];
        notHosts?: pulumi.Input<string>[];
        ports?: pulumi.Input<string>[];
        notPorts?: pulumi.Input<string>[];
        methods?: pulumi.Input<string>[];
        notMethods?: pulumi.Input<string>[];
        paths?: pulumi.Input<string>[];
        notPaths?: pulumi.Input<string>[];
    }

    export enum Action {
        ALLOW = "ALLOW",
        DENY = "DENY",
    }
}

export namespace DestinationRule {
    export interface DestinationRuleInput {
        metadata?: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
        spec: {
            host: pulumi.Input<string>;
            trafficPolicy?: pulumi.Input<TrafficPolicy>;
            subsets?: pulumi.Input<Subset>[];
            exportTo?: pulumi.Input<string>[];
        };
    }

    interface TrafficPolicy {
        loadBalancer?: pulumi.Input<LoadBalancerSettings>;
        connectionPool?: pulumi.Input<ConnectionPoolSettings>;
        outlierDetection?: pulumi.Input<OutlierDetection>;
        tls?: pulumi.Input<TLSSettings>;
        portLevelSettings?: pulumi.Input<PortTrafficPolicy>[];
    }

    interface LoadBalancerSettings {
        simple: pulumi.Input<string>; // https://istio.io/docs/reference/config/networking/destination-rule/#LoadBalancerSettings-SimpleLB
        consistentHash: pulumi.Input<ConsistentHashLB>;
    }

    interface ConsistentHashLB {
        httpHeaderName: pulumi.Input<string>;
        httpCookie: pulumi.Input<HTTPCookie>;
        useSourceIp: pulumi.Input<boolean>;
        minimumRingSize?: pulumi.Input<number>;
    }

    interface HTTPCookie {
        name: pulumi.Input<string>;
        path?: pulumi.Input<string>;
        ttl: pulumi.Input<Duration>;
    }

    interface Duration {
        value?: pulumi.Input<number>;
    }

    interface ConnectionPoolSettings {
        tcp: pulumi.Input<TCPSettings>;
        http: pulumi.Input<HTTPSettings>;
    }

    interface TCPSettings {
        maxConnections?: pulumi.Input<number>;
        connectTimeout?: pulumi.Input<Duration>;
        tcpKeepalive?: pulumi.Input<TcpKeepalive>;
    }

    interface TcpKeepalive {
        probes?: pulumi.Input<number>;
        time?: pulumi.Input<Duration>;
        interval?: pulumi.Input<Duration>;
    }

    interface HTTPSettings {
        http1MaxPendingRequests?: pulumi.Input<number>;
        http2MaxRequests?: pulumi.Input<number>;
        maxRequestsPerConnection?: pulumi.Input<number>;
        maxRetries?: pulumi.Input<number>;
        idleTimeout?: pulumi.Input<Duration>;
        h2UpgradePolicy?: pulumi.Input<string>; // https://istio.io/docs/reference/config/networking/destination-rule/#ConnectionPoolSettings-HTTPSettings-H2UpgradePolicy
    }

    interface OutlierDetection {
        consecutiveErrors?: pulumi.Input<number>;
        interval?: pulumi.Input<Duration>;
        baseEjectionTime?: pulumi.Input<Duration>;
        maxEjectionPercent?: pulumi.Input<number>;
        minHealthPercent?: pulumi.Input<number>;
    }

    interface TLSSettings {
        mode: pulumi.Input<string>; // https://istio.io/docs/reference/config/networking/destination-rule/#TLSSettings-TLSmode
        clientCertificate?: pulumi.Input<string>;
        privateKey?: pulumi.Input<string>;
        caCertificates?: pulumi.Input<string>;
        subjectAltNames?: pulumi.Input<string>[];
        sni?: pulumi.Input<string>;
    }

    interface PortTrafficPolicy {
        port?: pulumi.Input<number>;
        loadBalancer?: pulumi.Input<LoadBalancerSettings>;
        connectionPool?: pulumi.Input<ConnectionPoolSettings>;
        outlierDetection?: pulumi.Input<OutlierDetection>;
        tls?: pulumi.Input<TLSSettings>;
    }

    interface Subset {
        name: pulumi.Input<string>;
        labels?: pulumi.Input<Map<pulumi.Input<string>, pulumi.Input<string>>>;
        trafficPolicy?: pulumi.Input<TrafficPolicy>;
    }
}

export namespace EnvoyFilter {
    export interface EnvoyFilterInput {
        metadata?: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
        spec: {
            workloadSelector?: pulumi.Input<commons.WorkloadSelector>;
            configPatches: pulumi.Input<EnvoyConfigObjectPatch>;
        };
    }

    interface EnvoyConfigObjectPatch {
        applyTo?: pulumi.Input<ApplyTo>;
        match?: pulumi.Input<EnvoyConfigObjectMatch>;
        patch?: pulumi.Input<Patch>;
    }

    export enum ApplyTo {
        INVALID = "INVALID",
        LISTENER = "LISTENER",
        FILTER_CHAIN = "FILTER_CHAIN",
        NETWORK_FILTER = "NETWORK_FILTER",
        HTTP_FILTER = "HTTP_FILTER",
        ROUTE_CONFIGURATION = "ROUTE_CONFIGURATION",
        VIRTUAL_HOST = "VIRTUAL_HOST",
        HTTP_ROUTE = "HTTP_ROUTE",
        CLUSTER = "CLUSTER",
    }

    interface EnvoyConfigObjectMatch {
        context?: pulumi.Input<PatchContext>;
        proxy?: pulumi.Input<ProxyMatch>;
        listener: pulumi.Input<ListenerMatch>;
        routeConfiguration: pulumi.Input<RouteConfigurationMatch>;
        cluster: pulumi.Input<ClusterMatch>;
    }

    export enum PatchContext {
        ANY = "ANY",
        SIDECAR_INBOUND = "SIDECAR_INBOUND",
        SIDECAR_OUTBOUND = "SIDECAR_OUTBOUND",
        GATEWAY = "GATEWAY",
    }

    interface ProxyMatch {
        proxyVersion?: pulumi.Input<string>;
        metadata?: pulumi.Input<Map<pulumi.Input<string>, pulumi.Input<string>>>;
    }

    interface ListenerMatch {
        portNumber?: pulumi.Input<number>;
        filterChain?: pulumi.Input<FilterChainMatch>;
        name?: pulumi.Input<string>;
    }

    interface FilterChainMatch {
        name?: pulumi.Input<string>;
        sni?: pulumi.Input<string>;
        transportProtocol?: pulumi.Input<string>;
        applicationProtocols?: pulumi.Input<string>;
        filter?: pulumi.Input<FilterMatch>;
    }

    interface FilterMatch {
        name?: pulumi.Input<string>;
        subFilter?: pulumi.Input<SubFilterMatch>;
    }

    interface SubFilterMatch {
        name?: pulumi.Input<string>;
    }

    interface RouteConfigurationMatch {
        portNumber?: pulumi.Input<number>;
        portName?: pulumi.Input<string>;
        gateway?: pulumi.Input<string>;
        vhost?: pulumi.Input<VirtualHostMatch>;
        name?: pulumi.Input<string>;
    }

    interface VirtualHostMatch {
        name?: pulumi.Input<string>;
        route?: pulumi.Input<RouteMatch>;
    }

    interface RouteMatch {
        name?: pulumi.Input<string>;
        action?: pulumi.Input<Action>;
    }

    export enum Action {
        ANY = "ANY",
        ROUTE = "ROUTE",
        REDIRECT = "REDIRECT",
        DIRECT_RESPONSE = "DIRECT_RESPONSE",
    }

    interface ClusterMatch {
        portNumber?: pulumi.Input<number>;
        service?: pulumi.Input<string>;
        subset?: pulumi.Input<string>;
        name?: pulumi.Input<string>;
    }

    interface Patch {
        operation?: pulumi.Input<Operation>;
        value?: pulumi.Input<any>;
    }

    export enum Operation {
        INVALID = "INVALID",
        MERGE = "MERGE",
        ADD = "ADD",
        REMOVE = "REMOVE",
        INSERT_BEFORE = "INSERT_BEFORE",
        INSERT_AFTER = "INSERT_AFTER",
        INSERT_FIRST = "INSERT_FIRST",
    }
}

export namespace Gateway {
    export interface GatewayInput {
        metadata?: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
        spec: {
            servers: pulumi.Input<pulumi.Input<Server>[]>;
            selector?: {
                [labels: string]: pulumi.Input<any>;
            }
        };
    }

    interface Server {
        port: pulumi.Input<Port>;
        hosts: pulumi.Input<string>[];
        tls?: pulumi.Input<TLSOptions>;
        defaultEndpoint?: pulumi.Input<string>;
    }

    interface TLSOptions {
        httpsRedirect?: pulumi.Input<boolean>;
        mode?: pulumi.Input<string>; // https://istio.io/docs/reference/config/networking/gateway/#Server-TLSOptions-TLSmode
        serverCertificate?: pulumi.Input<string>;
        privateKey?: pulumi.Input<string>;
        caCertificates?: pulumi.Input<string>;
        credentialName?: pulumi.Input<string>;
        subjectAltNames?: pulumi.Input<string>[];
        verifyCertificateSpki?: pulumi.Input<string>[];
        verifyCertificateHash?: pulumi.Input<string>[];
        minProtocolVersion?: pulumi.Input<string>; // https://istio.io/docs/reference/config/networking/gateway/#Server-TLSOptions-TLSProtocol
        maxProtocolVersion?: pulumi.Input<string>; // https://istio.io/docs/reference/config/networking/gateway/#Server-TLSOptions-TLSProtocol
        cipherSuites?: pulumi.Input<string>[];
    }

    interface Port {
        number: pulumi.Input<number>;
        protocol: pulumi.Input<string>;
        name?: pulumi.Input<string>;
    }
}

export namespace RequestAuthentication {
    export interface RequestAuthenticationInput {
        metadata?: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
        spec: {
            selector?: pulumi.Input<commons.WorkloadSelector>;
            jwtRules?: pulumi.Input<JwtRule>[];
        };
    }

    interface JwtRule {
        issuer: pulumi.Input<string>;
        audiences?: pulumi.Input<string>[];
        jwksUri?: pulumi.Input<string>;
        jwks?: pulumi.Input<string>;
        fromHeaders?: pulumi.Input<JWTHeader>[];
        fromParams?: pulumi.Input<string>[];
        outputPayloadToHeader?: pulumi.Input<string>;
        forwardOriginalToken?: pulumi.Input<boolean>;
    }

    interface JWTHeader {
        name: pulumi.Input<string>;
        prefix?: pulumi.Input<string>;
    }
}

export namespace VirtualService {
    export interface VirtualServiceInput {
        metadata?: pulumi.Input<k8s.types.input.meta.v1.ObjectMeta>;
        spec: {
            hosts: pulumi.Input<pulumi.Input<string>[]>;
            gateways?: pulumi.Input<pulumi.Input<string>[]>;
            http?: pulumi.Input<pulumi.Input<HTTPRoute>[]>;
            tls?: pulumi.Input<pulumi.Input<TLSRoute>[]>;
            tcp?: pulumi.Input<pulumi.Input<TCPRoute>[]>;
            exportTo?: pulumi.Input<pulumi.Input<string>[]>;
        };
    }

    // HTTPRoute
    interface HTTPRoute {
        name?: pulumi.Input<string>;
        match?: pulumi.Input<HTTPMatchRequest>[];
        route?: pulumi.Input<HTTPRouteDestination>[];
        redirect?: pulumi.Input<HTTPRedirect>;
        rewrite?: pulumi.Input<HTTPRewrite>;
        timeout?: pulumi.Input<Duration>;
        retries?: pulumi.Input<HTTPRetry>;
        fault?: pulumi.Input<HTTPFaultInjection>;
        mirror?: pulumi.Input<Destination>;
        mirrorPercent?: pulumi.Input<number>;
        corsPolicy?: pulumi.Input<CorsPolicy>;
        headers?: pulumi.Input<HeadersIstio>;
    }

    interface HTTPMatchRequest {
        name?: pulumi.Input<string>;
        uri?: pulumi.Input<stringMatch>;
        scheme?: pulumi.Input<stringMatch>;
        method?: pulumi.Input<stringMatch>;
        authority?: pulumi.Input<stringMatch>;
        headers?: pulumi.Input<Map<pulumi.Input<string>, pulumi.Input<stringMatch>>>;
        port?: pulumi.Input<number>;
        sourceLabels?: pulumi.Input<Map<pulumi.Input<string>, pulumi.Input<string>>>;
        queryParams?: pulumi.Input<Map<pulumi.Input<string>, pulumi.Input<stringMatch>>>;
        ignoreUriCase?: pulumi.Input<boolean>;
    }

    type stringMatch = ExactstringMatch | PrefixstringMatch | RegexstringMatch;

    interface ExactstringMatch {
        exact: pulumi.Input<string>;
    }

    interface PrefixstringMatch {
        prefix: pulumi.Input<string>;
    }

    interface RegexstringMatch {
        regex: pulumi.Input<string>;
    }

    interface HTTPRouteDestination {
        destination: pulumi.Input<Destination>;
        weight?: pulumi.Input<number>;
        headers?: pulumi.Input<HeadersIstio>;
        removeResponseHeaders?: pulumi.Input<pulumi.Input<string>>[];
        appendResponseHeaders?: pulumi.Input<Map<pulumi.Input<string>, pulumi.Input<string>>>;
        removeRequestHeaders?: pulumi.Input<pulumi.Input<string>>[];
        appendRequestHeaders?: pulumi.Input<Map<pulumi.Input<string>, pulumi.Input<string>>>;
    }

    interface Destination {
        host: pulumi.Input<string>;
        subset?: pulumi.Input<string>;
        port?: pulumi.Input<PortSelector>;
    }

    interface PortSelector {
        number: pulumi.Input<number>;
    }

    interface HeadersIstio {
        request: pulumi.Input<HeaderOperations>;
        response: pulumi.Input<HeaderOperations>;
    }

    interface HeaderOperations {
        set: pulumi.Input<Map<pulumi.Input<string>, pulumi.Input<string>>>;
        add: pulumi.Input<Map<pulumi.Input<string>, pulumi.Input<string>>>;
        remove: pulumi.Input<string>[];
    }

    interface HTTPRedirect {
        uri?: pulumi.Input<string>;
        authority?: pulumi.Input<string>;
        redirectCode?: pulumi.Input<number>;
    }
    interface HTTPRewrite {
        uri?: pulumi.Input<string>;
        authority?: pulumi.Input<string>;
    }

    interface HTTPRetry {
        attempts: pulumi.Input<number>;
        perTryTimeout?: pulumi.Input<Duration>;
        retryOn?: pulumi.Input<string>;
    }

    interface HTTPFaultInjection {
        delay?: pulumi.Input<Delay>;
        abort?: pulumi.Input<Abort>;
    }

    interface Delay {
        fixedDelay: pulumi.Input<Duration>;
        percentage?: pulumi.Input<Percent>;
        percent?: pulumi.Input<number>;
    }

    interface Abort {
        httpStatus: pulumi.Input<number>;
        percentage?: pulumi.Input<Percent>;
        percent?: pulumi.Input<number>;
    }

    interface Percent {
        value?: pulumi.Input<number>;
    }
    interface Duration {
        value?: pulumi.Input<number>;
    }

    interface CorsPolicy {
        allowOrigin?: pulumi.Input<string>[];
        allowMethods?: pulumi.Input<string>[];
        allowHeaders?: pulumi.Input<string>[];
        exposeHeaders?: pulumi.Input<string>[];
        maxAge?: pulumi.Input<Duration>;
        allowCredentials?: pulumi.Input<boolean>;
    }

    // TLSRoute
    interface TLSRoute {
        match: pulumi.Input<TLSMatchAttributes>[];
        route?: pulumi.Input<RouteDestination>[];
    }

    interface TLSMatchAttributes {
        sniHosts: pulumi.Input<string>[];
        destinationSubnets?: pulumi.Input<string>[];
        port?: pulumi.Input<number>;
        sourceLabels?: pulumi.Input<Map<string, string>>;
        gateways?: pulumi.Input<string>[];
    }

    interface RouteDestination {
        destination: pulumi.Input<Destination>;
        weight: pulumi.Input<number>;
    }

    // TCPRoute
    interface TCPRoute {
        match?: pulumi.Input<string>;
        route?: pulumi.Input<string>;
    }
}
