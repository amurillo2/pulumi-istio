import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as inputs from "../../types/input";

export class AuthorizationPolicy extends k8s.apiextensions.CustomResource {

    constructor(name: string, props: inputs.AuthorizationPolicy.AuthorizationPolicyInput, opts?: pulumi.CustomResourceOptions) {
        super(name, {
            apiVersion: "security.istio.io/v1beta1",
            kind: "AuthorizationPolicy",
            metadata: props.metadata,
            spec: props.spec,
        }, opts);
    }
}
