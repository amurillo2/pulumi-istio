import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as inputs from "../../types/input";

export class DestinationRule extends k8s.apiextensions.CustomResource {

    constructor(name: string, props: inputs.DestinationRule.DestinationRuleInput, opts?: pulumi.CustomResourceOptions) {
        super(name, {
            apiVersion: "networking.istio.io/v1beta1",
            kind: "DestinationRule",
            metadata: props.metadata,
            spec: props.spec,
        }, opts);
    }
}
