import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as inputs from "../../types/input";

export class VirtualService extends k8s.apiextensions.CustomResource {

    constructor(name: string, props: inputs.VirtualService.VirtualServiceInput, opts?: pulumi.CustomResourceOptions) {
        super(name, {
            apiVersion: "networking.istio.io/v1beta1",
            kind: "VirtualService",
            metadata: props.metadata,
            spec: props.spec,
        }, opts);
    }
}
