import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as inputs from "../../types/input";

export class EnvoyFilter extends k8s.apiextensions.CustomResource {

    constructor(name: string, props: inputs.EnvoyFilter.EnvoyFilterInput, opts?: pulumi.CustomResourceOptions) {
        super(name, {
            apiVersion: "networking.istio.io/v1beta1",
            kind: "EnvoyFilter",
            metadata: props.metadata,
            spec: props.spec,
        }, opts);
    }
}
